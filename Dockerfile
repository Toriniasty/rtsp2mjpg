FROM sebp/lighttpd
RUN apk add --no-cache ffmpeg bash supervisor

COPY stream.sh /var/www/cgi-bin/stream
COPY frame.sh /var/www/cgi-bin/frame
COPY grab_frame.sh /usr/local/bin/grab_frame.sh

COPY lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY supervisord.conf /etc/supervisord.conf

RUN chmod +x /var/www/cgi-bin/stream /var/www/cgi-bin/frame /usr/local/bin/grab_frame.sh
RUN echo '* * * * * /usr/local/bin/grab_frame.sh' > /etc/crontabs/root

ENTRYPOINT ["supervisord", "--configuration", "/etc/supervisord.conf"]