# RTSP2MJPEG

## Info
This takes RTSP stream and converts to MJPEG as some software might require it.  

In principal it will open ffmpeg in background whenever stream will be requested and killed whenever the connection will be closed.

## Usage
Edit `docker-compose.yaml` file and change the `URL` to the RTSP URL of your stream.  
Additionaly comment out or change the value of `SKIP_FRAMES`. This is to skip certain amount of frames when generating still image(for example until stream will 'stabilise').  
Optionally if you want, adjust the port.  

## Stream / Still image
For the stream:  
`http://<your-docker-host-ip>:23123/cgi-bin/stream`  
For the still image(generated every minute if using `SKIP_FRAMES` option):  
`http://<your-docker-host-ip>:23123/cgi-bin/frame`  

## Reference
Based on:  
https://stackoverflow.com/questions/68368511/how-to-convert-rtsp-stream-to-mjpeg-http-using-ffmpeg  
https://stevethemoose.blogspot.com/2021/07/converting-rtsp-to-mjpeg-stream-on.html