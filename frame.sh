#!/bin/bash

echo "Content-Type: image/jpeg"
echo "Cache-Control: no-cache"
echo ""

if [[ -n "${SKIP_FRAMES}" ]] && [[ "${SKIP_FRAMES}" > 0 ]]; then
  cat /var/www/localhost/htdocs/image.jpg
else
  ffmpeg -i "${URL}" -vframes 1 -f image2pipe -an - > /var/www/localhost/htdocs/image.jpg
fi
