#!/bin/bash

if [[ -n "${SKIP_FRAMES}" ]] && [[ "${SKIP_FRAMES}" > 0 ]]; then
  ffmpeg -i "${URL}" -vframes 1 -vf "select=eq(n\,${SKIP_FRAMES})" -f image2pipe -an - > /var/www/localhost/htdocs/image.jpg
fi
